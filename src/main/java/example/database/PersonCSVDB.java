package example.database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.collections4.ListUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import example.beans.PersonImpl;
import example.helper.CSVFile;
import example.helper.CSVPattern;
import example.helper.CSVRecord;
import example.helper.ColorCodes;

/**
 * CSV based database for persons.
 * 
 * This class reads a given csv file and uses its entries as the db entries.
 * 
 * @author FLo
 *
 */
@ManagedBean
@ApplicationScoped
public class PersonCSVDB implements IPersonDB {

	protected static final Logger logger = LogManager.getLogger(PersonCSVDB.class);

	protected List<PersonImpl> persons;

	/**
	 * Creates a person db based on a csv file. The default csv file located in
	 * ${resources}/person.csv is used.
	 */
	public PersonCSVDB() {
		logger.info("Creating new instance of PersonCSVDB");
		String resourceName = "person.csv";
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		File csvFile = new File(classloader.getResource(resourceName).getFile());
		init(csvFile);
	}

	/**
	 * Creates a new instance of a person database based on a csv file.
	 * 
	 * @param csvFile the csvFile
	 */
	public PersonCSVDB(File csvFile) {
		init(csvFile);
	}

	/**
	 * Inits the csv file. Only used by the constructors.
	 */
	private void init(File csvFile) {
		logger.info(String.format("Reading content of csv file %s", csvFile.getName()));
		CSVFile theCsvFile = CSVFile.parse(csvFile, CSVPattern.ASSECOR_PERSONS_CSV_PATTERN);

		persons = ListUtils.emptyIfNull(new ArrayList<PersonImpl>());
		/*
		 * Read each entry and transform it into a person
		 */
		int id = 1;
		for (CSVRecord record : theCsvFile.getRecords()) {
			PersonImpl person = new PersonImpl();
			person.setNachname(record.getColumns().get(0));
			person.setVorname(record.getColumns().get(1));
			person.setZipcode(record.getColumns().get(2));
			person.setCity(record.getColumns().get(3));
			person.setColor(ColorCodes.getColor(record.getColumns().get(4)));
			person.setId(id);
			id++;

			persons.add(person);
		}
	}

	@Override
	public List<PersonImpl> getPersons() {
		return this.persons;
	}

	@Override
	public PersonImpl getPerson(Integer id) {
		for (PersonImpl person : persons) {
			if (person.getId().equals(id)) {
				return person;
			}
		}
		return null;
	}

	/**
	 * Adds a person to the csv File and stores it in the person entry.
	 */
	@Override
	public Integer addPerson(PersonImpl person) {
		Integer newId = persons.size() + 1;

		person.setId(newId);
		persons.add(person);

		return newId;
	}

}

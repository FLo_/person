package example.database;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Factory class for IPersonDB databases. Each database entity is accessible as
 * singleton
 * 
 * @author FLo
 *
 */
@Deprecated
public class PersonDBFactory {

	protected static final Logger logger = LogManager.getLogger(PersonDBFactory.class);

	private static IPersonDB personCsvDb;

	/**
	 * Returns the singleton instance of the person csv db.
	 * 
	 * @return
	 */
	public static IPersonDB getPersonCsvDb() {
		if (personCsvDb == null) {
			String resourceName = "person.csv";
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			File csvFile = new File(classloader.getResource(resourceName).getFile());
			personCsvDb = new PersonCSVDB();
		}

		return personCsvDb;
	}

}

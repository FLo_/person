package example.database;

import java.util.List;

import example.beans.PersonImpl;

/**
 * Interface for the person database
 * 
 * @author FLo
 *
 */
public interface IPersonDB {

	/**
	 * Returns all stored persons. The returned list can be empty in case no records
	 * are found.
	 * 
	 * @return the list of all persons
	 */
	public List<PersonImpl> getPersons();

	/**
	 * Returns the person with the given id
	 * 
	 * @param id the id of the person
	 * @return the person, null if no person could be found
	 */
	public PersonImpl getPerson(Integer id);

	/**
	 * Adds a person to the database. Returns the id for the newly created entry.
	 * 
	 * @param person
	 * @return the id of the person
	 */
	public Integer addPerson(PersonImpl person);

}

package example;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import example.beans.PersonImpl;
import example.database.IPersonDB;
import example.helper.PersonHelper;

/**
 * Root class for the Person endpoint
 * 
 * @author FLo
 *
 */
@Path("persons")
@ManagedBean
public class Person {

	@Inject
	IPersonDB personDb;

	/**
	 * Returns all persons with all details
	 * 
	 * @return list of all persons
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PersonImpl> getAll() {
		return personDb.getPersons();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public PersonImpl getUser(@PathParam("id") Integer id) {
		return personDb.getPerson(id);
	}

	@Path("/color/{color}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PersonImpl> getAllWithColor(@PathParam("color") String color) {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allPersonsWithPreferredColor = PersonHelper.filterPersonsWithPreferredColor(allPersons, color);

		return allPersonsWithPreferredColor;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addPerson(final PersonImpl person, @Context UriInfo uriInfo) {
		Integer id = personDb.addPerson(person);

		/*
		 * Build the uri for the response
		 */
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(Integer.toString(id));
		return Response.created(builder.build()).build();

	}

}

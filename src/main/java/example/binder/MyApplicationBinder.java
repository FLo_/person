package example.binder;

import javax.inject.Singleton;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import example.database.IPersonDB;
import example.database.PersonCSVDB;

public class MyApplicationBinder extends AbstractBinder {

	@Override
	protected void configure() {
		/*
		 * Bind the PersonCSVDB class as a singleton to the IPersonDB interface
		 */
		bind(PersonCSVDB.class).to(IPersonDB.class).in(Singleton.class);

	}

}

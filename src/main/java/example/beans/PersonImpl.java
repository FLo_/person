package example.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean for the Person datatype used by the example.Person resource as return
 * value
 * 
 * @author FLo
 *
 */
@XmlRootElement
public class PersonImpl {

	@XmlElement
	protected Integer id;

	@XmlElement
	protected String vorname;

	@XmlElement
	protected String nachname;

	@XmlElement
	protected String zipcode;

	@XmlElement
	protected String city;

	@XmlElement
	protected String color;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}

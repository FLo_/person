package example;

import org.glassfish.jersey.server.ResourceConfig;

import example.binder.MyApplicationBinder;

public class MyApplication extends ResourceConfig {
	public MyApplication() {
		register(new MyApplicationBinder());
		packages(true, "example");
	}
}

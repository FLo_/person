package example.helper;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Color codes for the person service stored in a hash map.
 * 
 * @author FLo
 *
 */
public class ColorCodes {

	private static final Map<String, String> COLOR_CODES = Stream
			.of(new String[][] { { "1", "Blau" }, { "2", "Grün" }, { "3", "Lila" }, { "4", "Rot" },
					{ "5", "Zitronengelb" }, { "6", "Türkis" }, { "7", "Weiß" }, })
			.collect(Collectors.toMap(data -> data[0], data -> data[1]));

	/**
	 * Returns the color to the given color code. If none is found null is returned
	 * 
	 * @param colorCode the color code
	 * @return the matching color
	 */
	public static String getColor(String colorCode) {
		return COLOR_CODES.get(colorCode);
	}
}

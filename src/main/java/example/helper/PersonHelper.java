package example.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;

import example.beans.PersonImpl;

/**
 * Helper class for utility methods on person objects.
 * 
 * For example this class provides helper methods to filter a list of persons
 * with their preferred color
 * 
 * @author FLo
 *
 */
public class PersonHelper {

	/**
	 * This methods filters all persons with their preferred color
	 * 
	 * @param allPersons list of all persons
	 * @param color      the preferred color
	 * @return the list of all persons that share the same preferred color
	 */
	public static List<PersonImpl> filterPersonsWithPreferredColor(List<PersonImpl> allPersons, String color) {
		List<PersonImpl> filteredPersons = ListUtils.emptyIfNull(new ArrayList<PersonImpl>());

		for (PersonImpl person : allPersons) {
			if (person.getColor().equals(color)) {
				filteredPersons.add(person);
			}
		}

		return filteredPersons;
	}

}

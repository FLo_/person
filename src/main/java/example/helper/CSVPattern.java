package example.helper;

import java.util.regex.Pattern;

/**
 * Provides CSV pattern for common used csv formats.
 * 
 * @author FLo
 *
 */
public class CSVPattern {

	/**
	 * Pattern for the ASSECOR Persons example webservice.
	 */
	public static final Pattern ASSECOR_PERSONS_CSV_PATTERN = Pattern
			.compile("([^\\d\\s]*),\\s([^\\d\\s]*),\\s(\\d*)\\s([^\\d]*),\\s([\\d]*)", Pattern.MULTILINE);

}

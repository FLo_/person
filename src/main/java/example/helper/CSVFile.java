package example.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CSVFile {

	protected static final Logger logger = LogManager.getLogger(CSVFile.class);

	/**
	 * List of all CSVRecords contained in the given CSVFile
	 */
	protected List<CSVRecord> records;

	/**
	 * Sets the value for this CSVFile
	 * 
	 * @param records
	 */
	public void setRecords(List<CSVRecord> records) {
		this.records = ListUtils.emptyIfNull(new ArrayList<CSVRecord>());

		this.records.addAll(records);
	}

	/**
	 * Creates a CSV File from the given file with a given pattern. The pattern is a
	 * java regex that specifies the whole match of a single record. Groups within
	 * the pattern defines the columns of each record. Example patterns are stored
	 * in the CSVPatterns class.
	 * 
	 * @param csvFile
	 * @return
	 */
	public static CSVFile parse(File csvFile, Pattern pattern) {
		Matcher matcher = null;
		try {
			matcher = pattern.matcher(FileUtils.readFileToString(csvFile, "UTF-8"));
		} catch (IOException e) {

			logger.info(String.format("Could not parse csv file %s | Using empty CSV File", csvFile.getName()), e);
			return new CSVFile();
		}

		try {
			String file = FileUtils.readFileToString(csvFile, "UTF-8");
			logger.info(String.format("Read file %s", file));
		} catch (IOException e) {
			logger.info(String.format("Could not read from file %s", csvFile.getName()));
		}

		List<CSVRecord> records = new ArrayList<CSVRecord>();

		/*
		 * For each match create a single record
		 */
		while (matcher.find()) {

			CSVRecord record = CSVRecord.fromMatch(matcher);

			records.add(record);
		}

		CSVFile theCsvFile = new CSVFile();

		theCsvFile.setRecords(records);

		return theCsvFile;

	}

	/**
	 * 
	 * CSV File constructor.
	 */
	protected CSVFile() {

	}

	public List<CSVRecord> getRecords() {
		return this.records;
	}

}

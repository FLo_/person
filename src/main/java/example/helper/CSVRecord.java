package example.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.commons.collections4.ListUtils;

/**
 * Helper class for a single csv record
 * 
 * @author FLo
 *
 */
public class CSVRecord {

	protected List<String> columns;

	/**
	 * Returns the columns
	 * 
	 * @return list of columns
	 */
	public List<String> getColumns() {
		return this.columns;
	}

	/**
	 * Resets all columns to the values of the given columns parameter
	 * 
	 * @param columns the new columns entry
	 */
	public void setColumns(List<String> columns) {

		this.columns = ListUtils.emptyIfNull(new ArrayList<String>());

		this.columns.addAll(columns);
	}

	/**
	 * Creates a single CSV Record from a given match. Each group represents a
	 * single column of the CSV Record Each column is stored as string.
	 * 
	 * @param fullMatch
	 * @return the CSVRecord of the match
	 */
	public static CSVRecord fromMatch(Matcher match) {
		List<String> record = ListUtils.emptyIfNull(new ArrayList<String>());
		for (int i = 1; i <= match.groupCount(); i++) {
			String column = match.group(i);
			record.add(column);
		}

		CSVRecord csvRecord = new CSVRecord();
		csvRecord.setColumns(record);
		return csvRecord;
	}

}

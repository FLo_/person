package example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import example.helper.CSVFile;
import example.helper.CSVPattern;

/**
 * Testclass for parsing the csv file
 * 
 * @author FLo
 *
 */
public class CSVParserTest {

	protected File csvFile;

	@BeforeEach
	void initAll() {
		String resourceName = "person.csv";

		ClassLoader classLoader = getClass().getClassLoader();
		csvFile = new File(classLoader.getResource(resourceName).getFile());
	}

	/**
	 * Test case that ensures that the given test file results in 10 records
	 */
	@Test
	public void shouldHave10Entries() {
		CSVFile parsedCsvFile = CSVFile.parse(csvFile, CSVPattern.ASSECOR_PERSONS_CSV_PATTERN);

		assertEquals(10, parsedCsvFile.getRecords().size());
	}
	

}

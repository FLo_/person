package example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import example.beans.PersonImpl;
import example.database.IPersonDB;
import example.database.PersonCSVDB;
import example.helper.PersonHelper;

public class PersonHelperTest {
	protected static final Logger logger = LogManager.getLogger(PersonCSVDBTest.class);

	protected IPersonDB personDb;

	@BeforeEach
	void initAll() {
		String resourceName = "person.csv";

		ClassLoader classLoader = getClass().getClassLoader();

		File csvFile = new File(classLoader.getResource(resourceName).getFile());

		personDb = new PersonCSVDB(csvFile);
	}

	@Test
	void shouldGetAllPersonsWithPreferredColorBlue() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allBluePersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Blau");

		assertEquals(2, allBluePersons.size());
		assertEquals("Müller", allBluePersons.get(0).getNachname());
		assertEquals("Hans", allBluePersons.get(0).getVorname());
		assertEquals("67742", allBluePersons.get(0).getZipcode());
		assertEquals("Lauterecken", allBluePersons.get(0).getCity());
		assertEquals("Blau", allBluePersons.get(0).getColor());

		assertEquals("Bart", allBluePersons.get(1).getNachname());
		assertEquals("Bertram", allBluePersons.get(1).getVorname());
		assertEquals("12313", allBluePersons.get(1).getZipcode());
		assertEquals("Wasweißich", allBluePersons.get(1).getCity());
		assertEquals("Blau", allBluePersons.get(1).getColor());
	}

	@Test
	void shouldGetAllPersonsWithPreferredColorGreen() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allGreenPersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Grün");

		assertEquals(3, allGreenPersons.size());
		assertEquals("Petersen", allGreenPersons.get(0).getNachname());
		assertEquals("Peter", allGreenPersons.get(0).getVorname());
		assertEquals("18439", allGreenPersons.get(0).getZipcode());
		assertEquals("Stralsund", allGreenPersons.get(0).getCity());
		assertEquals("Grün", allGreenPersons.get(0).getColor());

		assertEquals("Andersson", allGreenPersons.get(1).getNachname());
		assertEquals("Anders", allGreenPersons.get(1).getVorname());
		assertEquals("32132", allGreenPersons.get(1).getZipcode());
		assertEquals("Schweden - Bonus", allGreenPersons.get(1).getCity());
		assertEquals("Grün", allGreenPersons.get(1).getColor());

		assertEquals("Klaussen", allGreenPersons.get(2).getNachname());
		assertEquals("Klaus", allGreenPersons.get(2).getVorname());
		assertEquals("43246", allGreenPersons.get(2).getZipcode());
		assertEquals("Hierach", allGreenPersons.get(2).getCity());
		assertEquals("Grün", allGreenPersons.get(2).getColor());
	}

	@Test
	void shouldGetAllPersonsWithPreferredColorPurple() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allPurplePersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Lila");

		assertEquals(2, allPurplePersons.size());
		assertEquals("Johnson", allPurplePersons.get(0).getNachname());
		assertEquals("Johnny", allPurplePersons.get(0).getVorname());
		assertEquals("88888", allPurplePersons.get(0).getZipcode());
		assertEquals("Ausgedacht", allPurplePersons.get(0).getCity());
		assertEquals("Lila", allPurplePersons.get(0).getColor());

		assertEquals("Gerber", allPurplePersons.get(1).getNachname());
		assertEquals("Gerda", allPurplePersons.get(1).getVorname());
		assertEquals("76535", allPurplePersons.get(1).getZipcode());
		assertEquals("Woanders", allPurplePersons.get(1).getCity());
		assertEquals("Lila", allPurplePersons.get(1).getColor());
	}

	@Test
	void shouldGetAllPersonsWithPreferredColorRed() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allRedPersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Rot");

		assertEquals(1, allRedPersons.size());
		assertEquals("Millenium", allRedPersons.get(0).getNachname());
		assertEquals("Milly", allRedPersons.get(0).getVorname());
		assertEquals("77777", allRedPersons.get(0).getZipcode());
		assertEquals("Auch ausgedacht", allRedPersons.get(0).getCity());
		assertEquals("Rot", allRedPersons.get(0).getColor());

	}

	@Test
	void shouldGetAllPersonsWithPreferredColorLemon() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allLemonPersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Zitronengelb");

		assertEquals(1, allLemonPersons.size());
		assertEquals("Müller", allLemonPersons.get(0).getNachname());
		assertEquals("Jonas", allLemonPersons.get(0).getVorname());
		assertEquals("32323", allLemonPersons.get(0).getZipcode());
		assertEquals("Hansstadt", allLemonPersons.get(0).getCity());
		assertEquals("Zitronengelb", allLemonPersons.get(0).getColor());

	}

	@Test
	void shouldGetAllPersonsWithPreferredColorTurquoise() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allTurquoisePersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Türkis");

		assertEquals(1, allTurquoisePersons.size());
		assertEquals("Fujitsu", allTurquoisePersons.get(0).getNachname());
		assertEquals("Tastatur", allTurquoisePersons.get(0).getVorname());
		assertEquals("42342", allTurquoisePersons.get(0).getZipcode());
		assertEquals("Japan", allTurquoisePersons.get(0).getCity());
		assertEquals("Türkis", allTurquoisePersons.get(0).getColor());

	}

	@Test
	void shouldGetAllPersonsWithPreferredColorWhite() {
		List<PersonImpl> allPersons = personDb.getPersons();

		List<PersonImpl> allTurquoisePersons = PersonHelper.filterPersonsWithPreferredColor(allPersons, "Weiß");

		assertEquals(0, allTurquoisePersons.size());

	}
}

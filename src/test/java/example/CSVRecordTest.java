package example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.regex.Matcher;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import example.helper.CSVPattern;
import example.helper.CSVRecord;

public class CSVRecordTest {

	@BeforeEach
	public void initAll() {

	}

	/**
	 * Converts the value "Müller, Hans, 67742 Lauterecken, 1" in a single record
	 */
	@Test
	public void shouldReadASingleRecordFromMatcher() {
		String value = "Müller, Hans, 67742 Lauterecken, 1";

		Matcher matcher = CSVPattern.ASSECOR_PERSONS_CSV_PATTERN.matcher(value);

		while (matcher.find()) {
			CSVRecord record = CSVRecord.fromMatch(matcher);

			assertEquals(5, record.getColumns().size());
			List<String> stringRecord = record.getColumns();
			assertEquals("Müller", stringRecord.get(0));
			assertEquals("Hans", stringRecord.get(1));
			assertEquals("67742", stringRecord.get(2));
			assertEquals("Lauterecken", stringRecord.get(3));
			assertEquals("1", stringRecord.get(4));
		}

	}
	

	/**
	 * Converts the value "Millenium, Milly, 77777 Auch ausgedacht, 4" in a single record
	 */
	@Test
	public void shouldReadASingleRecordFromMatcherWithSpacesInStreetAddress() {
		String value = "Millenium, Milly, 77777 Auch ausgedacht, 4";

		Matcher matcher = CSVPattern.ASSECOR_PERSONS_CSV_PATTERN.matcher(value);

		while (matcher.find()) {
			CSVRecord record = CSVRecord.fromMatch(matcher);

			assertEquals(5, record.getColumns().size());
			List<String> stringRecord = record.getColumns();
			assertEquals("Millenium", stringRecord.get(0));
			assertEquals("Milly", stringRecord.get(1));
			assertEquals("77777", stringRecord.get(2));
			assertEquals("Auch ausgedacht", stringRecord.get(3));
			assertEquals("4", stringRecord.get(4));
		}

	}
	
	/**
	 * Converts the value "Andersson. Anders, 32132 Schweden - Bonus, 2" in a single record
	 */
	@Test
	public void shouldReadSSingleRecordFromMatcherWithSpecialSignsInAddress() {
		String value = "Andersson, Anders, 32132 Schweden - Bonus, 2";

		Matcher matcher = CSVPattern.ASSECOR_PERSONS_CSV_PATTERN.matcher(value);

		while (matcher.find()) {
			CSVRecord record = CSVRecord.fromMatch(matcher);

			assertEquals(5, record.getColumns().size());
			List<String> stringRecord = record.getColumns();
			assertEquals("Andersson", stringRecord.get(0));
			assertEquals("Anders", stringRecord.get(1));
			assertEquals("32132", stringRecord.get(2));
			assertEquals("Schweden - Bonus", stringRecord.get(3));
			assertEquals("2", stringRecord.get(4));
		}
	}
}

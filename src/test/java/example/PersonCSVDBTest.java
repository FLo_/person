package example;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import example.beans.PersonImpl;
import example.database.IPersonDB;
import example.database.PersonCSVDB;
import example.helper.ColorCodes;

public class PersonCSVDBTest {

	protected static final Logger logger = LogManager.getLogger(PersonCSVDBTest.class);

	/*
	 * Temp file used for the testcases
	 */
	@TempDir
	protected static File tmpCsvFile;

	protected IPersonDB personDb;

	@BeforeEach
	void initAll() {
		String resourceName = "person.csv";

		ClassLoader classLoader = getClass().getClassLoader();

		File csvFile = new File(classLoader.getResource(resourceName).getFile());

		try {
			FileUtils.copyFileToDirectory(csvFile, tmpCsvFile);
		} catch (IOException e) {
			logger.warn("Could not create a temporary copy of csv File", e);
			fail("Could not create a temporary copy of csv File", e);
		}

		personDb = new PersonCSVDB(csvFile);
	}

	/**
	 * Reads all entries from a given file and stores them in the database. This
	 * testcase succeeds if 10 entries are found
	 */
	@Test
	public void shouldReadAllEntriesFromGivenFile() {
		assertEquals(10, personDb.getPersons().size());
	}

	/**
	 * Reads the first entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadFirstEntry() {
		PersonImpl person = personDb.getPerson(1);

		assertEquals("Müller", person.getNachname());
		assertEquals("Hans", person.getVorname());
		assertEquals("67742", person.getZipcode());
		assertEquals("Lauterecken", person.getCity());
		assertEquals(ColorCodes.getColor("1"), person.getColor());
		assertEquals(1, person.getId());
	}

	/**
	 * Reads the second entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadSecondEntry() {
		PersonImpl person = personDb.getPerson(2);

		assertEquals("Petersen", person.getNachname());
		assertEquals("Peter", person.getVorname());
		assertEquals("18439", person.getZipcode());
		assertEquals("Stralsund", person.getCity());
		assertEquals(ColorCodes.getColor("2"), person.getColor());
		assertEquals(2, person.getId());
	}

	/**
	 * Reads the third entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadThirdEntry() {
		PersonImpl person = personDb.getPerson(3);

		assertEquals("Johnson", person.getNachname());
		assertEquals("Johnny", person.getVorname());
		assertEquals("88888", person.getZipcode());
		assertEquals("Ausgedacht", person.getCity());
		assertEquals(ColorCodes.getColor("3"), person.getColor());
		assertEquals(3, person.getId());
	}

	/**
	 * Reads the fourth entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadFourthEntry() {
		PersonImpl person = personDb.getPerson(4);

		assertEquals("Millenium", person.getNachname());
		assertEquals("Milly", person.getVorname());
		assertEquals("77777", person.getZipcode());
		assertEquals("Auch ausgedacht", person.getCity());
		assertEquals(ColorCodes.getColor("4"), person.getColor());
		assertEquals(4, person.getId());
	}

	/**
	 * Reads the fifth entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadFifthEntry() {
		PersonImpl person = personDb.getPerson(5);

		assertEquals("Müller", person.getNachname());
		assertEquals("Jonas", person.getVorname());
		assertEquals("32323", person.getZipcode());
		assertEquals("Hansstadt", person.getCity());
		assertEquals(ColorCodes.getColor("5"), person.getColor());
		assertEquals(5, person.getId());
	}

	/**
	 * Reads the sixth entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadSixthEntry() {
		PersonImpl person = personDb.getPerson(6);

		assertEquals("Fujitsu", person.getNachname());
		assertEquals("Tastatur", person.getVorname());
		assertEquals("42342", person.getZipcode());
		assertEquals("Japan", person.getCity());
		assertEquals(ColorCodes.getColor("6"), person.getColor());
		assertEquals(6, person.getId());
	}

	/**
	 * Reads the seventh entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadSeventhEntry() {
		PersonImpl person = personDb.getPerson(7);

		assertEquals("Andersson", person.getNachname());
		assertEquals("Anders", person.getVorname());
		assertEquals("32132", person.getZipcode());
		assertEquals("Schweden - Bonus", person.getCity());
		assertEquals(ColorCodes.getColor("2"), person.getColor());
		assertEquals(7, person.getId());
	}

	/**
	 * Reads the eighth entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadEighthEntry() {
		PersonImpl person = personDb.getPerson(8);

		assertEquals("Bart", person.getNachname());
		assertEquals("Bertram", person.getVorname());
		assertEquals("12313", person.getZipcode());
		assertEquals("Wasweißich", person.getCity());
		assertEquals(ColorCodes.getColor("1"), person.getColor());
		assertEquals(8, person.getId());
	}

	/**
	 * Reads the ninth entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadNinthEntry() {
		PersonImpl person = personDb.getPerson(9);

		assertEquals("Gerber", person.getNachname());
		assertEquals("Gerda", person.getVorname());
		assertEquals("76535", person.getZipcode());
		assertEquals("Woanders", person.getCity());
		assertEquals(ColorCodes.getColor("3"), person.getColor());
		assertEquals(9, person.getId());
	}

	/**
	 * Reads the tenth entry of the database and ensures that the values are
	 * correct.
	 */
	@Test
	public void shouldReadTenthEntry() {
		PersonImpl person = personDb.getPerson(10);

		assertEquals("Klaussen", person.getNachname());
		assertEquals("Klaus", person.getVorname());
		assertEquals("43246", person.getZipcode());
		assertEquals("Hierach", person.getCity());
		assertEquals(ColorCodes.getColor("2"), person.getColor());
		assertEquals(10, person.getId());
	}

	@Test
	public void shouldReturnNullIfNoPersonIsFoundWithId() {
		PersonImpl person = personDb.getPerson(11);

		assertNull(person);
	}

	/**
	 * Tests if a new entry is stored in the csv file. The id field is managed by
	 * the PersonCSVDB class
	 */
	@Test
	public void shouldAddANewEntry() {
		PersonImpl personToAdd = new PersonImpl();

		personToAdd.setCity("My City");
		personToAdd.setColor(ColorCodes.getColor("7"));
		personToAdd.setNachname("New Nachname");
		personToAdd.setVorname("New Vorname");
		personToAdd.setZipcode("12345");

		Integer id = personDb.addPerson(personToAdd);

		assertEquals(11, id);

		PersonImpl personAdded = personDb.getPerson(id);

		assertEquals("New Nachname", personAdded.getNachname());
		assertEquals("New Vorname", personAdded.getVorname());
		assertEquals("12345", personAdded.getZipcode());
		assertEquals("My City", personAdded.getCity());
		assertEquals(ColorCodes.getColor("7"), personAdded.getColor());
		assertEquals(11, personAdded.getId());

	}

}

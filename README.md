# Person Webservice

This project contains the sourcecode for a webservice that manages data for persons. Personal data is stored in a csv file. The webservice offers REST-endpoints to read the person data, filter persons by color and add new person data.

## Getting Started

The webservice is created as a JavaEE application. It can be either build and deployed on a webservice container of your choice or directly executed on a docker container. This tutorial explains both steps.

### Prerequisites

If you want to run the webservice within a docker container you need to install the docker environment for your system. Please see [Docker](http://www.docker.com) for more information.

### Install on a webservice container

Create the war archive via maven

```bash
mvn clean package
```

The war file is created under the folder `./target/person-service-webapp.war`. Use this war archive to deploy it on your webservice container of your choice.

### Install via docker

This project contains a dockerfile with the necessary steps to deploy and run the application on a tomcat webservice.

First you need to create the war archive with maven

```bash
mvn clean package
```

Now you can use docker to create the docker image

```
docker build -t person-webservice-app .
```

Run the docker image

```
docker run -p 8080:8080 person-webservice-app
```

Access the webservice with the following url

```
http://localhost:8080/person-service-webapp/
```

The endpoint to retreive all persons is located under

```
http://localhost:8080/person-service-webapp/webapi/persons
```

### Using Azure Cloud

This project contains deployment instructions for the azure cloud. You can access the latest release under

```
http://person-webservice-webapp.azurewebsites.net/person-service-webapp/webapi/persons
```

### Using Postman

This project contains some example requests for [Postman](https://www.getpostman.com). Use the `Assecor - Person Service.postman_collection.json` file to import the collection in Postman. You need to define a new environment with the properties `host` and `port`. You can use the values `person-webservice-webapp.azurewebsites.net` and `80` to access the latest build deployed on Azure Cloud.

### Testcases

A number of testcases are available in the `./src/test/java/example` directory. You can execute them with

```
mvn clean test
```






